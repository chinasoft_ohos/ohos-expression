# ohos-expression
 
#### 项目介绍
- 项目名称：ohos-expression
- 所属系列：openharmony的第三方组件适配移植
- 功能：自定义表情包的库
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 2.2.8

#### 效果演示

![效果演示](./printscreen/expression.gif "截图1") 


#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:expression:1.0.0')
    ......  
 }
 ```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

将自定义view添加到XML中，使用方法如下:
```
<com.example.expressionlibrary.ExpressionShowFragment
        ohos:id="$+id:main"
        ohos:height="match_parent"
        ohos:width="match_parent"/>
```

完整调用:

```
        TextField mText = (TextField) findComponentById(ResourceTable.Id_test_edit);
        ExpressionShowFragment expressionShowFragment = (ExpressionShowFragment) findComponentById(ResourceTable.Id_main);
        expressionShowFragment.input(mText);
```




关于 [ExpressionShowFragment ](https://gitee.com/chinasoft_ohos/ohos-expression/blob/master/expressionlibrary/src/main/java/com/example/expressionlibrary/ExpressionShowFragment.java)


| Method                      | Description                      |
| :-------------------------- | :------------------------------- |
| input                        | 设置显示组件textField             | 


 


#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异 

#### 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT

#### 版权和许可信息

[![license](http://img.shields.io/badge/license-Apache2.0-brightgreen.svg?style=flat)](https://github.com/mabeijianxi/android-expression/blob/master/LICENSE)

