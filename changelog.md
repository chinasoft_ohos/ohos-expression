## 1.0.0
发布1.0.0 release版本

## 0.0.1-SNAPSHOT

expression组件openharmony beta版本

#### api差异

* 使用ComponentContainer替换Fragment
* 输入框改为了text，可以加载图片表情和文字以及表情
* 表情包使用utf-8的字符替换原图

#### 已实现功能

* 该库基本功能已全部实现

#### 未实现功能

* 无