package com.mabeijianxi.sample;

import com.mabeijianxi.jianxiexpression.util.TextUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ExampleTest {



    @Test
    public void TextUtils2() {
        boolean isEmpty =  TextUtils.isEmpty("12");
        Assert.assertEquals(false,isEmpty);
    }
    @Test
    public void TextUtils21() {
        boolean isEmpty =  TextUtils.isEmpty("");
        System.out.println(isEmpty);
        Assert.assertEquals(true,isEmpty);
    }
    @Test
    public void TextUtils22() {
        boolean isEmpty =  TextUtils.isEmpty(null);
        Assert.assertEquals(true,isEmpty);
    }

    @Test
    public void TextUtils23() {
        List list = new ArrayList();
        boolean isEmpty =  TextUtils.isEmpty(list);
        Assert.assertEquals(true,isEmpty);
    }

    @Test
    public void TextUtils24() {
        List list = new ArrayList();
        list.add("1");
        boolean isEmpty =  TextUtils.isEmpty(list);
        Assert.assertEquals(false,isEmpty);
    }




}
