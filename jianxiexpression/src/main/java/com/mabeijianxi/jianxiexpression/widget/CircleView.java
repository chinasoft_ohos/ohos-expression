package com.mabeijianxi.jianxiexpression.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * 圆形
 */
public class CircleView extends Component implements Component.DrawTask{
    private final Paint mPaintPageFill = new Paint();
    private final Paint mPaintStroke = new Paint();
    /**
     * 默认选中
     */
    public int defaultChose = 0;
    /**
     *默认数量
     */
    public volatile int defaultCount = 0;

    /**
     *
     * @param context
     */
    public CircleView(Context context) {
        super(context);
    }

    /**
     *
     * @param context
     * @param attrSet
     */
    public CircleView(Context context, AttrSet attrSet) {
        super(context, attrSet,0);

        mPaintPageFill.setStyle(Paint.Style.FILL_STYLE);
        mPaintPageFill.setAntiAlias(true);
        mPaintPageFill.setColor(new Color(0xff333333));
        mPaintPageFill.setStrokeWidth(10);

        mPaintStroke.setStyle(Paint.Style.STROKE_STYLE);
        mPaintStroke.setAntiAlias(true);
        mPaintStroke.setColor(new Color(0xff333333));
        mPaintStroke.setAlpha(0.2f);
        mPaintStroke.setStrokeWidth(5);
        addDrawTask(this::onDraw);
    }

    /**
     *
     * @param context
     * @param attrSet
     * @param defaultStyle
     */
    public CircleView(Context context, AttrSet attrSet, int defaultStyle) {
        super(context,attrSet,defaultStyle);
    }


    @Override
    public void addDrawTask(DrawTask task) {
        super.addDrawTask(task);
        task.onDraw(this, mCanvasForTaskOverContent);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        double dX = 270;
        double dY = 30;
        double pageFillRadius  = 10;
        for (int i = 0;i< defaultCount;i++) {
            if(i == defaultCount /2) {
                canvas.drawCircle((float) dX, (float)dY, (float)pageFillRadius, (i == defaultChose)?mPaintPageFill:mPaintStroke);
            } else if ( i == defaultCount/2 -1 && (defaultCount/2 -1) >= 0) {
                canvas.drawCircle((float)(dX - 50),(float) dY, (float)pageFillRadius, (i == defaultChose) ? mPaintPageFill : mPaintStroke);
            } else if(i == defaultCount/2 +1 && (defaultCount/2 +1) <= defaultCount) {
                canvas.drawCircle((float)(dX + 50), (float)dY, (float)pageFillRadius, (i == defaultChose)?mPaintPageFill:mPaintStroke);
            } else if(i == defaultCount/2 -2 && (defaultCount/2 -2) >= 0) {
                canvas.drawCircle((float)(dX - 100),(float)dY, (float)pageFillRadius, (i == defaultChose) ? mPaintPageFill : mPaintStroke);
            } else if(i == defaultCount/2 +2 && (defaultCount/2 +2) <= defaultCount) {
                canvas.drawCircle((float)(dX + 100),(float) dY, (float)pageFillRadius, (i == defaultChose)?mPaintPageFill:mPaintStroke);
            }
        }
    }
}
