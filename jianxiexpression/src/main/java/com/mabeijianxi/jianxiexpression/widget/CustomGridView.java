package com.mabeijianxi.jianxiexpression.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.TableLayout;
import ohos.app.Context;

/**
 * 自定义gridview
 */
public  class CustomGridView extends TableLayout {

    /**
     *
     * @param context
     */
    public CustomGridView(Context context) {
        super(context);
        init();
    }

    /**
     *
     * @param context
     * @param attrSet
     */
    public CustomGridView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    /**
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public CustomGridView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    private void init() {
        setOrientation(HORIZONTAL);
        setRowCount(3);
        setColumnCount(7);
    }
}
