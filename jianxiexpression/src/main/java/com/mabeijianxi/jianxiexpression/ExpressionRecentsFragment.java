package com.mabeijianxi.jianxiexpression;

import com.mabeijianxi.jianxiexpression.core.ExpressionCache;
import ohos.agp.components.*;
import ohos.app.Context;

/**
 *最近使用
 */
public class ExpressionRecentsFragment extends ComponentContainer {
    private Component component;
    private PageSlider pageSlider;
    private Context mContext;
    private String[][] data = null;
    private GridAdapter gridAdapter;

    private ExpressionClickListener mExpressionClickListener;
    private ExpressionDeleteClickListener mExpressionDeleteClickListener;

    /**
     * ExpressionRecentsFragment
     * @param context
     */
    public ExpressionRecentsFragment(Context context) {
        super(context);
        mContext = context;
        init(mContext);
    }

    /**
     * ExpressionRecentsFragment
     * @param context
     * @param attrSet
     */
    public ExpressionRecentsFragment(Context context, AttrSet attrSet) {
        super(context, attrSet);
        mContext = context;
        init(mContext);
    }

    /**
     *ExpressionRecentsFragment
     * @param context
     * @param attrSet
     * @param styleName
     */
    public ExpressionRecentsFragment(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * getAdapter
     * @return gridAdapter
     */
    public PageSliderProvider getAdapter() {
        return gridAdapter;
    }

    /**
     * notifyDataNow
     * @param data
     */
    public void notifyDataNow(String[][] data) {
        this.data = data.clone();
        if(gridAdapter != null) {
            gridAdapter.setData(this.data);
            gridAdapter.notifyDataChanged();
            init(mContext);
        }
    }

    /**
     *init
     * @param context
     */
    private void  init(Context context) {
        component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_expression_recents_fragment,null,false);
        pageSlider = (PageSlider) component.findComponentById(ResourceTable.Id_second_pager);
        pageSlider.setOrientation(Component.HORIZONTAL);
        pageSlider.setSlidingPossible(true);
        data = new String[][]{
                ExpressionCache.getRecentExpression()};
        gridAdapter = new GridAdapter(mContext,data,mExpressionClickListener,mExpressionDeleteClickListener);
        pageSlider.setProvider(gridAdapter);

        addComponent(component);
    }

    /**
     *setExpressionClickListener
     * @param expressionClickListener
     */
    public void setExpressionClickListener(ExpressionClickListener expressionClickListener) {
        this.mExpressionClickListener = expressionClickListener;
    }

    /**
     * 表情点击回调
     */
    public interface ExpressionClickListener {
        void expressionClick(String str);
    }

    /**
     * setExpressionDeleteClickListener
     * @param expressionDeleteClickListener
     */
    public void setExpressionDeleteClickListener(ExpressionDeleteClickListener expressionDeleteClickListener) {
        this.mExpressionDeleteClickListener = expressionDeleteClickListener;
    }

    /**
     * 删除点击回调
     */
    public interface ExpressionDeleteClickListener {
        void expressionDeleteClick();
    }

}
