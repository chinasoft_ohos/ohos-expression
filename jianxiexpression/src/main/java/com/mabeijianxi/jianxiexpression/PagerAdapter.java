package com.mabeijianxi.jianxiexpression;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 *  页面适配器
 */
public class PagerAdapter extends PageSliderProvider {
    private List<ComponentContainer> listBaseFragment = new ArrayList<>();

    public PagerAdapter(Context context, List<ComponentContainer> list) {
        this.listBaseFragment = list;
    }

    @Override
    public int getCount() {
        return listBaseFragment.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component component = null;
        if (componentContainer == null) {
            return component;
        }
        if (0 <= position && position < listBaseFragment.size()) {
            component = listBaseFragment.get(position);

        }

        componentContainer.addComponent(component);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object obj) {
        if (componentContainer == null) {
            return;
        }
        if (obj instanceof Component) {
            componentContainer.removeComponent((Component) obj);
        }
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }
}
