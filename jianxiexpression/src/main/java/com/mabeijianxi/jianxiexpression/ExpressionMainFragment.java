package com.mabeijianxi.jianxiexpression;

import com.mabeijianxi.jianxiexpression.core.ExpressionCache;
import com.mabeijianxi.jianxiexpression.util.PxUtil;
import com.mabeijianxi.jianxiexpression.util.TextUtils;
import com.mabeijianxi.jianxiexpression.widget.CircleView;
import com.mabeijianxi.jianxiexpression.widget.CustomGridView;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;

import java.io.IOException;

/**
 *  主页
 */
public class ExpressionMainFragment extends ComponentContainer implements PageSlider.PageChangedListener {

    private Component component;
    private PageSlider pageSlider;
    private Context mContext;
    private int current = 0;
    private String[][] data = new String[][] {
            ExpressionCache.getPage_1(),
            ExpressionCache.getPage_2(),
            ExpressionCache.getPage_3(),
            ExpressionCache.getPage_4(),
            ExpressionCache.getPage_5()
    };

    private ExpressionClickListener mExpressionClickListener;
    private ExpressionDeleteClickListener mExpressionDeleteClickListener;
    private CircleView circleView;

    /**
     * ExpressionMainFragment
     * @param context
     */
    public ExpressionMainFragment(Context context) {
        super(context);
        mContext = context;
        init(mContext);
    }

    /**
     * ExpressionMainFragment
     * @param context
     * @param attrSet
     */
    public ExpressionMainFragment(Context context, AttrSet attrSet) {
        super(context, attrSet);
        mContext = context;
        init(mContext);
    }

    /**
     * ExpressionMainFragment
     * @param context
     * @param attrSet
     * @param styleName
     */
    public ExpressionMainFragment(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * handler  滑动页面的时候
     * @param handler
     */
    public void setCurrentPage(boolean handler) {
        if(handler) {
            circleView.invalidate();
            return;
        } else {
            pageSlider.setCurrentPage(0);
            current = 0;
            circleView.defaultChose = 0;
            circleView.invalidate();
        }
    }

    /**
     * init
     * @param context
     */
    private void init(Context context) {
        component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_sliderpager_item,null,false);
        pageSlider = (PageSlider) component.findComponentById(ResourceTable.Id_main_slider);
        pageSlider.setWidth(PxUtil.screenWidth());
        circleView = (CircleView) component.findComponentById(ResourceTable.Id_circle_show);
        circleView.defaultCount = 5;
        circleView.invalidate();
        pageSlider.setOrientation(Component.HORIZONTAL);
        pageSlider.setSlidingPossible(true);
        pageSlider.setCurrentPage(0);
        pageSlider.addPageChangedListener(this);
        pageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return data.length;
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                Component component = null;
                if(componentContainer == null) {
                    return component;
                };
                component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_layout,null,false);
                CustomGridView customGridView = (CustomGridView) component.findComponentById(ResourceTable.Id_show_grid);
//                customGridView.setWidth(PxUtil.screenWidth());
//                customGridView.setAlignmentType();
                current = i;
                for (int j = 0; j < 21; j++) {
                    Text image = new Text(mContext);
                    Image delete  = new Image(mContext);
                    //需要使用屏幕的宽度计算间距
                    image.setWidth((int) PxUtil.vp2px(40));
                    image.setHeight((int) PxUtil.vp2px(40));
                    image.setTextSize((int) PxUtil.vp2px(25));
                    image.setMarginsLeftAndRight(15, 15);
                    image.setMarginsTopAndBottom(10, 10);
                    delete.setWidth((int) PxUtil.vp2px(40));
                    delete.setHeight((int) PxUtil.vp2px(40));
                    delete.setMarginsLeftAndRight(0, 15);
                    delete.setMarginsTopAndBottom(10, 15);
                    final String[] pagerCoure = data[current];
                    if (pagerCoure[j].length() > 0 && j != 20) {
                        image.setText(TextUtils.uni2emoji(data[current][j]));
                    } else if(j == 20) {
                        try {
                            delete.setPixelMap(ImageSource.create(getResourceManager().getResource(ResourceTable.Media_del_icon), new ImageSource.SourceOptions()).createPixelmap(null));
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (NotExistException e) {
                            e.printStackTrace();
                        }
//                        image.setText(TextUtils.uni2emoji("274e"));
                    }
                    customGridView.addComponent(((j == 20) ? delete : image), j);
                        int position = j;
                        image.setClickedListener(new ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                if(pagerCoure[position] != null) {
                                    if (mExpressionClickListener != null) {
                                        mExpressionClickListener.expressionClick(pagerCoure[position]);
                                    }
                                }
                            }
                        });

                        delete.setClickedListener(new ClickedListener() {
                            @Override
                            public void onClick(Component component) {
                                if(position == 20){
                                    if(mExpressionDeleteClickListener!=null){
                                        mExpressionDeleteClickListener.expressionDeleteClick();
                                    }
                                    return;
                                }
                            }
                        });
                    }
                componentContainer.addComponent(component);
                return component;
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object obj) {
                if (componentContainer == null) {
                    return;
                }
                if (obj instanceof Component) {
                    componentContainer.removeComponent((Component) obj);
                }
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        addComponent(component);
    }

    /**
     * setExpressionClickListener
     * @param expressionClickListener
     */
    public void setExpressionClickListener(ExpressionClickListener expressionClickListener) {
        this.mExpressionClickListener = expressionClickListener;
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {

    }

    @Override
    public void onPageSlideStateChanged(int i) {

    }

    @Override
    public void onPageChosen(int i) {
        if(circleView != null) {
            circleView.defaultChose = i;
            circleView.invalidate();
        }
    }

    /**
     * 表情点击回调
     */
    public interface ExpressionClickListener {
        void expressionClick(String str);
    }

    /**
     * setExpressionDeleteClickListener
     * @param expressionDeleteClickListener
     */
    public void setExpressionDeleteClickListener(ExpressionDeleteClickListener expressionDeleteClickListener) {
        this.mExpressionDeleteClickListener = expressionDeleteClickListener;
    }

    /**
     * 删除点击回调
     */
    public interface ExpressionDeleteClickListener {
        void expressionDeleteClick();
    }
}
