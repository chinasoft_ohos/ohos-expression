package com.mabeijianxi.jianxiexpression.core;


import java.util.HashMap;

/**
 * Created by jian on 2016/6/23.
 * mabeijianxi@gmail.com
 */
public class ExpressionCache {
    //    TODO 每页数据资源，可自己定义，自己更改
    static String[] page_1 = new String[21];
    static String[] page_2 = new String[21];
    static String[] page_3 = new String[21];
    static String[] page_4 = new String[21];
    static String[] page_5 = new String[21];

    public static String[] getPage_1(){
        page_1[0] = "1f60a";
        page_1[1] = "1f603";
        page_1[2] = "1f601";
        page_1[3] = "1f602";
        page_1[4] = "1f620";
        page_1[5] = "1f61e";
        page_1[6] = "1f630";
        page_1[7] = "1f624";
        page_1[8] = "1f61d";
        page_1[9] = "1f628";
        page_1[10] = "1f61a";
        page_1[11] = "1f623";
        page_1[12] = "1f60c";
        page_1[13] = "1f60f";
        page_1[14] = "1f614";
        page_1[15] = "1f633";
        page_1[16] = "1f61c";
        page_1[17] = "1f631";
        page_1[18] = "1f621";
        page_1[19] = "1f62d";
        page_1[20] = "";
        return page_1.clone();
    }

    public static String[] getPage_2(){
        page_2[0] = "1f606";
        page_2[1] = "1f60b";
        page_2[2] = "1f632";
        page_2[3] = "1f629";
        page_2[4] = "1f622";
        page_2[5] = "1f62a";
        page_2[6] = "1f635";
        page_2[7] = "1f637";
        page_2[8] = "1f605";
        page_2[9] = "1f618";
        page_2[10] = "1f613";
        page_2[11] = "1f624";
        page_2[12] = "1f60d";
        page_2[13] = "1f625";
        page_2[14] = "1f609";
        page_2[15] = "1f62b";
        page_2[16] = "1f604";
        page_2[17] = "1f61e";
        page_2[18] = "1f616";
        page_2[19] = "1f63a";
        page_2[20] = "";
        return page_2.clone();
    }

    public static String[] getPage_3(){
        page_3[0] = "1f638";
        page_3[1] = "1f639";
        page_3[2] = "1f63d";
        page_3[3] = "1f63b";
        page_3[4] = "1f63f";
        page_3[5] = "1f63e";
        page_3[6] = "1f63c";
        page_3[7] = "1f640";
        page_3[8] = "1f648";
        page_3[9] = "1f64a";
        page_3[10] = "1f436";
        page_3[11] = "1f431";
        page_3[12] = "1f43c";
        page_3[13] = "1f430";
        page_3[14] = "1f437";
        page_3[15] = "1f411";
        page_3[16] = "1f466";
        page_3[17] = "1f467";
        page_3[18] = "1f4cc";
        page_3[19] = "1f47d";
        page_3[20] = "";
        return page_3.clone();
    }

    public static String[] getPage_4(){
        page_4[0] = "1f191";
        page_4[1] = "1f192";
        page_4[2] = "1f19a";
        page_4[3] = "1f232";
        page_4[4] = "1f233";
        page_4[5] = "1f234";
        page_4[6] = "1f235";
        page_4[7] = "1f236";
        page_4[8] = "1f21a";
        page_4[9] = "1f237";
        page_4[10] = "1f238";
        page_4[11] = "1f239";
        page_4[12] = "1f22f";
        page_4[13] = "1f23a";
        page_4[14] = "3299";
        page_4[15] = "3297";
        page_4[16] = "1f250";
        page_4[17] = "1f6bb";
        page_4[18] = "1f6be";
        page_4[19] = "1f251";
        page_4[20] = "";
        return page_4.clone();
    }

    public static String[] getPage_5(){
        page_5[0] = "1f684";
        page_5[1] = "1f685";
        page_5[2] = "1f697";
        page_5[3] = "1f699";
        page_5[4] = "1f68c";
        page_5[5] = "1f68f";
        page_5[6] = "1f6a2";
        page_5[7] = "2708";
        page_5[8] = "26f5";
        page_5[9] = "1f689";
        page_5[10] = "1f680";
        page_5[11] = "1f6a4";
        page_5[12] = "1f695";
        page_5[13] = "1f69a";
        page_5[14] = "1f692";
        page_5[15] = "1f691";
        page_5[16] = "1f693";
        page_5[17] = "26fd";
        page_5[18] = "";
        page_5[19] = "";
        page_5[20] = "";
        return page_5.clone();
    }

    /**
     * 所有的表情资源id缓存，增加效率
     */
    private static HashMap<String, Integer> allExpressionTable = new HashMap<String, Integer>();

 /*   static {
            allExpressionTable.put(page_1[0], ResourceTable.Media_aa_keai
        );
        allExpressionTable.put(page_1[1], ResourceTable.Media_ab_haha
        );
        allExpressionTable.put(page_1[2], ResourceTable.Media_ac_xixi
        );
        allExpressionTable.put(page_1[3], ResourceTable.Media_ad_xiaoku
        );
        allExpressionTable.put(page_1[4], ResourceTable.Media_ae_sikao
        );
        allExpressionTable.put(page_1[5], ResourceTable.Media_af_numa
        );
        allExpressionTable.put(page_1[6], ResourceTable.Media_ag_xu
        );
        allExpressionTable.put(page_1[7], ResourceTable.Media_ah_guzhang
        );
        allExpressionTable.put(page_1[8], ResourceTable.Media_ai_zuohengheng
        );
        allExpressionTable.put(page_1[9], ResourceTable.Media_aj_youhengheng
        );
        allExpressionTable.put(page_1[10], ResourceTable.Media_ak_qinqin
        );
        allExpressionTable.put(page_1[11], ResourceTable.Media_al_yiwen
        );
        allExpressionTable.put(page_1[12], ResourceTable.Media_am_baibai
        );
        allExpressionTable.put(page_1[13], ResourceTable.Media_an_hehe
        );
        allExpressionTable.put(page_1[14], ResourceTable.Media_ao_yinxian
        );
        allExpressionTable.put(page_1[15], ResourceTable.Media_ap_haixiu
        );
        allExpressionTable.put(page_1[16], ResourceTable.Media_aq_jiyan
        );
        allExpressionTable.put(page_1[17], ResourceTable.Media_ar_dalian
        );
        allExpressionTable.put(page_1[18], ResourceTable.Media_as_nu
        );
        allExpressionTable.put(page_1[19], ResourceTable.Media_at_lei
        );


        allExpressionTable.put(page_2[0], ResourceTable.Media_ba_bishi
        );
        allExpressionTable.put(page_2[1], ResourceTable.Media_bb_chanzui
        );
        allExpressionTable.put(page_2[2], ResourceTable.Media_bc_chijing
        );
        allExpressionTable.put(page_2[3], ResourceTable.Media_bd_dahaqi
        );
        allExpressionTable.put(page_2[4], ResourceTable.Media_be_beishang
        );
        allExpressionTable.put(page_2[5], ResourceTable.Media_bf_bizui
        );
        allExpressionTable.put(page_2[6], ResourceTable.Media_bg_ding
        );
        allExpressionTable.put(page_2[7], ResourceTable.Media_bh_ganmao
        );
        allExpressionTable.put(page_2[8], ResourceTable.Media_bi_han
        );
        allExpressionTable.put(page_2[9], ResourceTable.Media_bj_aini
        );
        allExpressionTable.put(page_2[10], ResourceTable.Media_bk_heixian
        );
        allExpressionTable.put(page_2[11], ResourceTable.Media_bl_heng
        );
        allExpressionTable.put(page_2[12], ResourceTable.Media_bm_huaxin
        );
        allExpressionTable.put(page_2[13], ResourceTable.Media_bn_kelian
        );
        allExpressionTable.put(page_2[14], ResourceTable.Media_bo_ku
        );
        allExpressionTable.put(page_2[15], ResourceTable.Media_bp_kun
        );
        allExpressionTable.put(page_2[16], ResourceTable.Media_bq_landelini
        );
        allExpressionTable.put(page_2[17], ResourceTable.Media_br_qian
        );
        allExpressionTable.put(page_2[18], ResourceTable.Media_bs_shayan
        );
        allExpressionTable.put(page_2[19], ResourceTable.Media_bt_shengbing
        );


        allExpressionTable.put(page_3[0], ResourceTable.Media_ca_shiwang
        );
        allExpressionTable.put(page_3[1], ResourceTable.Media_cb_shuijiao
        );
        allExpressionTable.put(page_3[2], ResourceTable.Media_cc_zhuakuang
        );
        allExpressionTable.put(page_3[3], ResourceTable.Media_cd_taikaixin
        );
        allExpressionTable.put(page_3[4], ResourceTable.Media_ce_touxiao
        );
        allExpressionTable.put(page_3[5], ResourceTable.Media_cf_tu
        );
        allExpressionTable.put(page_3[6], ResourceTable.Media_cg_wabishi
        );
        allExpressionTable.put(page_3[7], ResourceTable.Media_ch_weiqu
        );
        allExpressionTable.put(page_3[8], ResourceTable.Media_ci_yun
        );
        allExpressionTable.put(page_3[9], ResourceTable.Media_cj_shuai
        );
        allExpressionTable.put(page_3[10], ResourceTable.Media_ck_doge
        );
        allExpressionTable.put(page_3[11], ResourceTable.Media_cl_miao
        );
        allExpressionTable.put(page_3[12], ResourceTable.Media_cm_xiongmao
        );
        allExpressionTable.put(page_3[13], ResourceTable.Media_cn_tuzi
        );
        allExpressionTable.put(page_3[14], ResourceTable.Media_co_zhutou
        );
        allExpressionTable.put(page_3[15], ResourceTable.Media_cp_shenshou
        );
        allExpressionTable.put(page_3[16], ResourceTable.Media_cq_nanhaier
        );
        allExpressionTable.put(page_3[17], ResourceTable.Media_cr_nvhaier
        );
        allExpressionTable.put(page_3[18], ResourceTable.Media_cs_feizao
        );
        allExpressionTable.put(page_3[19], ResourceTable.Media_ct_aoteman

        );

        allExpressionTable.put(page_4[0], ResourceTable.Media_da_geili
        );
        allExpressionTable.put(page_4[1], ResourceTable.Media_db_jiong
        );
        allExpressionTable.put(page_4[2], ResourceTable.Media_dc_meng
        );
        allExpressionTable.put(page_4[3], ResourceTable.Media_dd_shenma
        );
        allExpressionTable.put(page_4[4], ResourceTable.Media_de_v5
        );
        allExpressionTable.put(page_4[5], ResourceTable.Media_df_xi
        );
        allExpressionTable.put(page_4[6], ResourceTable.Media_dg_zhi
        );
        allExpressionTable.put(page_4[7], ResourceTable.Media_dh_buyao
        );
        allExpressionTable.put(page_4[8], ResourceTable.Media_di_good
        );
        allExpressionTable.put(page_4[9], ResourceTable.Media_dj_haha
        );
        allExpressionTable.put(page_4[10], ResourceTable.Media_dk_lai
        );
        allExpressionTable.put(page_4[11], ResourceTable.Media_dl_ok
        );
        allExpressionTable.put(page_4[12], ResourceTable.Media_dm_ruo
        );
        allExpressionTable.put(page_4[13], ResourceTable.Media_dn_woshou
        );
        allExpressionTable.put(page_4[14], ResourceTable.Media_do_ye
        );
        allExpressionTable.put(page_4[15], ResourceTable.Media_dp_zan
        );
        allExpressionTable.put(page_4[16], ResourceTable.Media_dq_zuoyi
        );
        allExpressionTable.put(page_4[17], ResourceTable.Media_dr_shangxin
        );
        allExpressionTable.put(page_4[18], ResourceTable.Media_ds_xin
        );
        allExpressionTable.put(page_4[19], ResourceTable.Media_dt_dangao
        );


        allExpressionTable.put(page_5[0], ResourceTable.Media_ea_feiji
        );
        allExpressionTable.put(page_5[1], ResourceTable.Media_eb_ganbei
        );
        allExpressionTable.put(page_5[2], ResourceTable.Media_ec_huatong
        );
        allExpressionTable.put(page_5[3], ResourceTable.Media_ed_lazhu
        );
        allExpressionTable.put(page_5[4], ResourceTable.Media_ee_liwu
        );
        allExpressionTable.put(page_5[5], ResourceTable.Media_ef_lvsidai
        );
        allExpressionTable.put(page_5[6], ResourceTable.Media_eg_weibo
        );
        allExpressionTable.put(page_5[7], ResourceTable.Media_eh_weiguan
        );
        allExpressionTable.put(page_5[8], ResourceTable.Media_ei_yinyue
        );
        allExpressionTable.put(page_5[9], ResourceTable.Media_ej_zhaoxiangji
        );
        allExpressionTable.put(page_5[10], ResourceTable.Media_ek_zhong
        );
        allExpressionTable.put(page_5[11], ResourceTable.Media_el_weifeng
        );
        allExpressionTable.put(page_5[12], ResourceTable.Media_em_xianhua
        );
        allExpressionTable.put(page_5[13], ResourceTable.Media_en_taiyang
        );
        allExpressionTable.put(page_5[14], ResourceTable.Media_eo_yueliang
        );
        allExpressionTable.put(page_5[15], ResourceTable.Media_ep_fuyun
        );
        allExpressionTable.put(page_5[16], ResourceTable.Media_eq_xiayu
        );
        allExpressionTable.put(page_5[17], ResourceTable.Media_er_shachenbao
        );
    } */

    /**
     * 最近使用表情缓存
     */
    private static String[] recentExpression = new String[21];

    public static String[] getRecentExpression() {
        String[] strings = recentExpression;
        return strings;
    }

    public static HashMap<String, Integer> getAllExpressionTable() {
        return allExpressionTable;
    }
}