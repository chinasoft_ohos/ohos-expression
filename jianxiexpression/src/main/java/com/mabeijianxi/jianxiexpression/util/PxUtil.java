/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mabeijianxi.jianxiexpression.util;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.text.DecimalFormat;

/**
 * px转换
 */
public class PxUtil {
    private static Display display;

    /**
     * initContext
     * @param context
     */
    public static void initContext(Context context) {
        display = DisplayManager.getInstance().getDefaultDisplay(context).get();
    }

    /**
     * screenWidth
     * @return int
     */
    public static int screenWidth() {
        return display.getAttributes().width;
    }

    /**
     * screenHeight
     * @return int
     */
    public static int screenHeight() {
        return display.getAttributes().height;
    }


    /**
     *vp2px
     * @param vp
     * @return float
     */
    public static double vp2px(double vp) {
        float dpi = display.getAttributes().densityPixels;
        return (double)vp * (double) dpi + 0.5f * (vp >= 0 ? 1 : -1);
    }


    /**
     * 格式化数字(保留一位小数)
     *
     * @param num 数字
     * @return String
     */
    public static String formatNum(int num) {
        DecimalFormat format = new DecimalFormat("0");
        return format.format(num);
    }

    /**
     * 格式化数字(保留两位小数)
     *
     * @param num 数字
     * @return String
     */
    public static String formatNum(float num) {
        if (((int) num * 1000) == (int) (num * 1000)) {
            //如果是一个整数
            return String.valueOf((int) num);
        }
        DecimalFormat format = new DecimalFormat("0.00");
        return format.format(num);
    }
}
