package com.mabeijianxi.jianxiexpression;


import com.mabeijianxi.jianxiexpression.util.PxUtil;
import com.mabeijianxi.jianxiexpression.util.TextUtils;
import com.mabeijianxi.jianxiexpression.widget.CustomGridView;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;

import java.io.IOException;

/**
 * gridview适配器
 */
public class GridAdapter extends PageSliderProvider {
    private String [][] data = null;
    private Context mContext;
    private ExpressionRecentsFragment.ExpressionClickListener mExpressionClickListener;
    private ExpressionRecentsFragment.ExpressionDeleteClickListener mExpressionDeleteClickListener;

    public GridAdapter(Context context, String[][] datas, ExpressionRecentsFragment.ExpressionClickListener expressionClickListener,
                       ExpressionRecentsFragment.ExpressionDeleteClickListener expressionDeleteClickListener) {
        this.mContext = context;
        this.data = datas.clone();
        this.mExpressionClickListener = expressionClickListener;
        this.mExpressionDeleteClickListener = expressionDeleteClickListener;
    }

    /**
     * setData
     * @param data
     */
    public void setData(String[][] data) {
        this.data = data.clone();
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        Component component = null;
        if(componentContainer == null) {
            return component;
        }
        component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_recents_layout,null,false);
        CustomGridView customGridView = (CustomGridView) component.findComponentById(ResourceTable.Id_recent_grid);
        for (int j = 0; j < data[0].length; j++) {
            Text image = new Text(mContext);
            Image delete  = new Image(mContext);
            //需要使用屏幕的宽度计算间距
            image.setWidth((int) PxUtil.vp2px(40));
            image.setHeight((int) PxUtil.vp2px(40));
            image.setTextSize((int) PxUtil.vp2px(25));
            image.setMarginsLeftAndRight(15, 15);
            image.setMarginsTopAndBottom(10, 10);
            delete.setWidth((int) PxUtil.vp2px(40));
            delete.setHeight((int) PxUtil.vp2px(40));
            delete.setMarginsLeftAndRight(0, 15);
            delete.setMarginsTopAndBottom(10, 10);
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(0xffffffff));
            final String[] pagerCoure = data[0];
            if (pagerCoure[j] != null && j != 20) {
                image.setText(TextUtils.uni2emoji(pagerCoure[j]));
                image.setBackground(shapeElement);
            } else if(j == 20) {
                try {
                    delete.setPixelMap(ImageSource.create(mContext.getResourceManager().getResource(ResourceTable.Media_del_icon), new ImageSource.SourceOptions()).createPixelmap(null));
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                }
            }
            customGridView.addComponent(((j == 20) ? delete : image), j);
            int position = j;
            image.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if(mExpressionClickListener != null && pagerCoure[position] != null && pagerCoure[position].length() > 0) {
                        mExpressionClickListener.expressionClick(pagerCoure[position]);
                    }
                }
            });
            delete.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if(position == 20){
                        if(mExpressionDeleteClickListener!=null){
                            mExpressionDeleteClickListener.expressionDeleteClick();
                        }
                        return;
                    }
                }
            });
        }
        componentContainer.addComponent(component);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object obj) {
        if (componentContainer == null) {
            return;
        }
        if (obj instanceof Component) {
            componentContainer.removeComponent((Component) obj);
        }
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }
}
